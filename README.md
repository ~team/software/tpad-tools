
The tpad-tools package – README file
=================================
An openrc service dedicated to Thinkpads

Overview
--------
A fork of [x200-my](https://git.robertalessi.net/x200-my/about/) 
tpad-tools is a simple openrc script primarily designed to make the
distinctive whining noise of Thinkpads disappear.  But it can
also be used on other laptops.

It is written to be used on [Hyperbola
GNU/Linux-libre](https://www.hyperbola.info) but it should work on
other Arch-based + openrc distributions.

Privacy Settings
----------------
It also provides a “Privacy settings” section: if `enable_privacy` is
set to `true`, then the wired internet interface that may or may not
be connected to the internet will have its MAC address spoofed at
startup. (The wireless interfaces must also be spoofed, but this can
be handled by wpa_supplicant: see below.)  Additionally, tpad-tools will
delete `/etc/dhcpcd.duid`, which will make `dhcpcd` generate a new
DUID-LLT based file with an updated timestamp and the spoofed
link-layer address of the network interface that is connected to the
DHCP device at the time that the DUID is generated.

It must be noted that when `enable_privacy` is set to `true` `tpad-tools`
turns off the wifi radio at computer startup and each time it is put
to sleep.  The wifi radio has to be turned on or turned back on
manually by pressing `Fn-F5`


Backup
------
Finally tpad-tools has the ability to backup essential files or
directories in any mounted disk or flash drive. Of course, the backup 
process is triggered on certain conditions only:

1. The option must be enabled in the `/etc/conf.d/tpad-tools` file.
2. The disk must be mounted.
3. An (empty) `dobackup` file must be present in the backup directory.

Then, once these conditions are met, the backup process is triggered
whenever the service is stopped or by pressing the `ThinkVantage`.  It additionally generates a list of
installed packages in case the disk should die and everything should
be reinstalled.

Other Commands
---------------
pressing `ThinkVantage` button will refresh the screen to remove the whining noise on thinkpad x200.
pressing `FN+F1` will reset internet connections using the privacy commands to end up with a new mac and ip address.
pressing `Power button` will poweroff your system.

License and Disclamer
=====================
tpad-tools - An openrc service dedicated to Thinkpads
Copyright (C) 2021  rachad <rachad@hyperbola.info>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

Installation
============
1. Clone this repository locally.
2. Do `makepkg`.
3. Install the generated tpad-tools package as root with `pacman -U`.
4. Add acpid to run-level default `doas rc-update add acpid default`.
5. Edit `/etc/conf.d/tpad-tools`.

Use and recommended configuration
---------------------------------
It is advisable to use tpad-tools in conjunction with `wpa_supplicant`
and `dhcpcd` only.  In addition, `wpa_gui` and `dhcpcd-ui` are also
recommended.

For this to work properly, it is important to have these two lines in
`/etc/wpa_supplicant/wpa_supplicant.conf`:

````
mac_addr=1
preassoc_mac_addr=1
````

And these settings in `/etc/dhcpcd.conf`:

````
hostname "localhost"
hostname_short
nooption 4 7 14 17 19 20 21 27 40 41 42 69 70 71 72 73 75 76
````

More information in `tpad-tools` initd file:
<https://git.hyperbola.info:50100/~~rachad/software/tpad-tools.git/tree/tpad-tools.confd>