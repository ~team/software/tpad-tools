exts := md html confd initd install sh
findopts := $(foreach ext,$(exts),-or -iname "*.$(ext)")

all: clean pkg

pkg: clean
	makepkg

clean:
	rm -rf .backup
	find -iname "*~" | xargs rm -rf
	find ./* -type d | xargs rm -rf
	find ./* -type f -iname "makefile" -or -iname "PKGBUILD" $(findopts) > ls-R
	rsync -avPr --files-from=ls-R . .backup
	rm -rf *
	cp -p -r .backup/* .
	rm -rf .backup
	pandoc -s README.md

.PHONY: all pkg clean
