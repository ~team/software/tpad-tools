#!/bin/bash
# Default acpi script that takes an entry for all actions

case "$1" in
    button/sleep)
        case "$2" in
            SLPB|SBTN)
                logger 'SleepButton pressed'
		rc-service tpad-tools restart
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
    button/lid)
        case "$3" in
            close)
                logger 'LID closed'
                ;;
            open)
                logger 'LID opened'
		rc-service tpad-tools restart
                ;;
            *)
                logger "ACPI action undefined: $3"
                ;;
        esac
        ;;
     button/vendor)
        case "$2" in
            VNDR)
                logger 'ThinkVantage pressed'
		/etc/init.d/tpad-tools backup_refresh
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
     button/fnf1)
        case "$2" in
            FNF1)
                logger 'fnf1 button pressed'
		/etc/init.d/tpad-tools reset_connections
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
     button/power)
        case "$2" in
            PBTN)
                logger 'power button pressed'
                /sbin/openrc-shutdown -p
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
esac

